use std::fs;
fn main() {
    let contents = fs::read_to_string("input.txt").expect("couldn't read file");
    let mut total: u32 = 0;
    for line in contents.lines() {
        let sack = Rucksack::new(line);
        total += score(dupe(&sack.first, &sack.second));
    }
    println!("part one: {total}",);

    let mut lines = contents.lines();
    let mut p2_total = 0;
    loop {
        let elf1: &str;
        match lines.next() {
            Some(s) => elf1 = s,
            None => break,
        }
        let elf2 = lines.next().expect("bad number of lines");
        let elf3 = lines.next().expect("bad number of lines");
        p2_total += score(triple_dupe(elf1, elf2, elf3));
    }

    println!("part two: {p2_total}",);
}

struct Rucksack {
    first: String,
    second: String,
}

impl Rucksack {
    fn new(base_string: &str) -> Self {
        let len = base_string.len();
        let (first, second) = base_string.split_at(len / 2);
        Self {
            first: first.to_owned(),
            second: second.to_owned(),
        }
    }
}
fn dupe(left: &str, right: &str) -> char {
    for character in left.chars() {
        if right.contains(character) {
            return character;
        }
    }

    for character in right.chars() {
        if left.contains(character) {
            return character;
        }
    }
    return '1';
}

fn triple_dupe(first: &str, second: &str, third: &str) -> char {
    for character in first.chars() {
        if second.contains(character) && third.contains(character) {
            return character;
        }
    }

    for character in second.chars() {
        if first.contains(character) && third.contains(character) {
            return character;
        }
    }
    return '1';
}

fn score(c: char) -> u32 {
    if c.is_uppercase() {
        return (c as u32) - 38;
    }
    return (c as u32) - 96;
}
