use std::cmp::Ordering;
use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filepath = &args[1];
    let contents = fs::read_to_string(filepath).expect("couldn't read it");

    let mut elves: Vec<Elf> = Vec::new();
    elves.push(Elf {
        id_no: elves.len(),
        calories: 0,
    });
    for line in contents.lines() {
        let elf = Elf {
            id_no: elves.len(),
            calories: 0,
        };
        match line {
            "" => elves.push(elf),
            _ => {
                elves.last_mut().unwrap().calories =
                    elves.last().unwrap().calories + line.parse::<u64>().expect("not a number")
            }
        }
    }

    elves.sort_by(|a, b| a.calories.cmp(&b.calories));
    //part one
    let calories = elves.last().unwrap().calories;
    println!("{calories}");
    elves.reverse();
    let multical = &elves[0..3].into_iter().fold(0, |acc, x| acc + x.calories);

    println!("{multical}");
}

struct Elf {
    id_no: usize,
    calories: u64,
}
