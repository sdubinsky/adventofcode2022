use std::fs;
fn main() {
    let contents = fs::read_to_string("input.txt").expect("couldn't read file");
    let mut total: i32 = 0;

    for line in contents.lines() {
        total += score(
            line.chars().nth(0).expect("bad"),
            line.chars().last().expect("bad"),
        );
    }
    println!("first round: {total}");

    total = 0;
    for line in contents.lines() {
        let their_move = line.chars().nth(0).expect("bad");
        let our_move = choose_move(their_move, line.chars().last().expect("bad"));
        println!("{line}",);
        println!("their move {their_move}, our move: {our_move}",);
        total += score(their_move, our_move)
    }

    println!("second round: {total}");
}

fn choose_move(a: char, result: char) -> char {
    match (a, result) {
        ('A', 'X') => 'Z',
        ('B', 'X') => 'X',
        ('C', 'X') => 'Y',
        ('A', 'Y') => 'X',
        ('B', 'Y') => 'Y',
        ('C', 'Y') => 'Z',
        ('A', 'Z') => 'Y',
        ('B', 'Z') => 'Z',
        ('C', 'Z') => 'X',
        _ => 'A',
    }
}

fn score(a: char, b: char) -> i32 {
    match (a, b) {
        ('A', 'X') => 4,
        ('B', 'X') => 1,
        ('C', 'X') => 7,
        ('A', 'Y') => 8,
        ('B', 'Y') => 5,
        ('C', 'Y') => 2,
        ('A', 'Z') => 3,
        ('B', 'Z') => 9,
        ('C', 'Z') => 6,
        _ => 0,
    }
}
