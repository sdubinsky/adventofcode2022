use std::fs;
fn main() {
    let raw_input = fs::read_to_string("input.txt").expect("couldn't read file");
    let input = raw_input.lines();
    let mut trees: Vec<Vec<Tree>> = Vec::new();

    for (i, line) in input.enumerate() {
        trees.push(Vec::new());
        for c in line.chars() {
            trees[i].push(Tree {
                height: c.to_string().parse::<i32>().unwrap(),
                visible: true,
                scenic: 0,
            });
        }
    }

    for row in 0..trees.len() {
        for column in 0..(trees[row].len()) {
            let mut left_vis = true;
            let mut top_vis = true;
            let mut bottom_vis = true;
            let mut right_vis = true;
            let size = trees[row].len();

            for edge in 0..column {
                if trees[row][edge].height >= trees[row][column].height {
                    left_vis = false;
                    break;
                }
            }

            for edge in 0..row {
                if trees[edge][column].height >= trees[row][column].height {
                    top_vis = false;
                    break;
                }
            }

            for edge in column + 1..size {
                if trees[row][edge].height >= trees[row][column].height {
                    right_vis = false;
                    break;
                }
            }

            for edge in row + 1..size {
                if trees[edge][column].height >= trees[row][column].height {
                    bottom_vis = false;
                    break;
                }
            }
            trees[row][column].visible = top_vis || right_vis || left_vis || bottom_vis;
        }
    }

    let mut count: usize = 0;
    for row in trees {
        for tree in row {
            if tree.visible {
                count += 1;
            }
        }
    }
    println!("part one {count}");


    let new_input = raw_input.lines();
    let mut new_trees: Vec<Vec<Tree>> = Vec::new();
    for (i, line) in new_input.enumerate() {
        new_trees.push(Vec::new());
        for c in line.chars() {
            new_trees[i].push(Tree {
                height: c.to_string().parse::<i32>().unwrap(),
                visible: true,
                scenic: 0,
            });
        }
    }

    for row in 0..new_trees.len() {
        for column in 0..(new_trees[row].len()) {
            let mut left_vis = 0;
            let mut top_vis = 0;
            let mut bottom_vis = 0;
            let mut right_vis = 0;
            let size = new_trees[row].len();

            for edge in 0..column {
                left_vis += 1;
                if new_trees[row][column - edge - 1].height >= new_trees[row][column].height {
                    break;
                }
            }

            for edge in 0..row {
                top_vis += 1;
                if new_trees[row - edge - 1][column].height >= new_trees[row][column].height {
                    break;
                }
            }

            for edge in column + 1..size {
                right_vis += 1;
                if new_trees[row][edge].height >= new_trees[row][column].height {
                    break;
                }
            }

            for edge in row + 1..size {
                bottom_vis += 1;
                if new_trees[edge][column].height >= new_trees[row][column].height {
                    break;
                }
            }
            new_trees[row][column].scenic = top_vis * right_vis * left_vis * bottom_vis;
        }
    }
    let scenic = new_trees.iter().map(|row| row.iter().map(|tree| tree.scenic).max()).max();
    println!("part two: {}", scenic.unwrap().unwrap());
}

pub struct Tree {
    height: i32,
    visible: bool,
    scenic: i32,
}
