use std::collections::hash_map::HashMap;
use std::fs;
fn main() {
    let input = fs::read_to_string("input.txt").expect("couldn't read file");
    let mut lines = input.lines();
    let mut stacks = parse_stack(lines);
    lines = input.lines();
    loop {
        let x = lines.next().unwrap();
        if x == "" {
            break;
        }
    }
    for line in lines {
        let mut parts = line.split(" ");
        let count = parts.nth(1).unwrap().parse::<i32>().unwrap();
        let from = parts.nth(1).unwrap().parse::<i32>().unwrap() - 1;
        let mut holder: Vec<String> = Vec::new();
        let to = parts.nth(1).unwrap().parse::<i32>().unwrap() - 1;
        let mut temp: String;
        for _ in 0..count {
            temp = stacks.entry(from).or_default().pop().unwrap();
            holder.push(temp);
        }
        holder.reverse();
        for i in holder {
            println!("adding {i}",);
            stacks.entry(to).or_default().push(i);
        }
    }

    println!("part 2",);
    let mut results: Vec<(i32, String)> = Vec::new();
    for (k, v) in stacks.iter_mut() {
        results.push((*k, v.pop().unwrap()));
    }
    results.sort_by_key(|a| a.0);
    for result in results {
        print!("{}", result.1);
    }
    println!("",);
}

fn parse_stack<'a>(mut items: impl Iterator<Item = &'a str>) -> HashMap<i32, Vec<String>> {
    let mut item: i32 = -1;
    let mut location = 1;
    let mut map: HashMap<i32, Vec<String>> = HashMap::new();
    while let Some(line) = items.next() {
        if line == "" {
            break;
        }
        while let Some(obj) = line.chars().nth(location) {
            location += 4;
            item += 1;
            if obj == ' ' || obj.is_digit(10) {
                continue;
            }
            map.entry(item).or_insert(Vec::new()).push(obj.to_string());
        }
        location = 1;
        item = -1;
    }
    for (_, v) in map.iter_mut() {
        v.reverse();
    }
    map
}
