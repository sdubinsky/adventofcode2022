use std::fs;
fn main() {
    let contents = fs::read_to_string("input.txt").expect("couldn't read file");
    let base = contents.lines().map(|x| {
        x.split(',')
            .flat_map(|y| y.split("-"))
            .map(|z| z.parse::<i32>().unwrap())
    });
    let total1 = base.clone().map(|x| contains(x)).sum::<i32>();
    println!("part 1: {total1}",);

    let total2 = base.clone().map(|x| overlaps(x)).sum::<i32>();
    println!("part 2: {total2}",);
}

fn contains(items: impl Iterator<Item = i32>) -> i32 {
    return match items.collect::<Vec<i32>>().as_slice() {
        [a, b, c, d] => {
            if (a <= c && b >= d) || (c <= a && d >= b) {
                1
            } else {
                0
            }
        }
        _ => -1,
    };
}

fn overlaps(items: impl Iterator<Item = i32>) -> i32 {
    return match items.collect::<Vec<i32>>().as_slice() {
        [a, b, c, d] => {
            if (a <= c && b >= c) || (a <= d && b >= d) || (c <= a && d >= b) || (c <= b && d >= a)
            {
                1
            } else {
                0
            }
        }
        _ => -1,
    };
}
