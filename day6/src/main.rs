use std::io::Result;
use std::io::{self, Read};
fn main() -> Result<()> {
    let mut stdin = io::stdin().bytes();
    while let Some(chr) = stdin.next() {
        let c = chr.unwrap() as char;
        println!("{c}",);
        if c == '\0' {
            break;
        }
    }
    Ok(())
}
