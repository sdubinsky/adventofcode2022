use std::io;
#[allow(unused_variables)]
fn main() {
    /*
    FS enum that can either be a dir or a file
    directory struct that holds a name and a list of FS objects
    file struct that holds a name and a size
    */
    let input = io::stdin();
    let content = input.lines().map(|a| a.unwrap());
    let mut top: Node = Node::Dir {
        name: "/".to_string(),
        contents: Vec::new(),
    };
    let mut path: Vec<String> = Vec::new();
    let mut node: &mut Node = &mut top;

    for line in content {
        let mut words = line.split(" ");
        match words.next() {
            Some("$") => {
                match words.next().unwrap() {
                    "cd" => {
                        let dirname = words.next().unwrap().to_string();
                        if dirname == ".." {
                            // dbg!(top.clone());
                            node = &mut top;
                            let mut new_path = path.clone();
                            path = Vec::new();
                            new_path.pop();
                            new_path.reverse();
                            // dbg!(new_path.clone());
                            while let Some(dir) = new_path.pop() {
                                node = node.find(dir.as_str());
                                path.push(dir);
                            }
                            continue;
                        }
                        if dirname == "/" {
                            node = &mut top;
                            path = Vec::new();
                        } else {
                            path.push(dirname.clone());
                            node = node.find(dirname.as_str());
                        }
                    }
                    _ => { /* don't need to do anything for ls*/ }
                }
            }
            Some("dir") => {
                let name = words.next().unwrap().to_string();
                node.add(Node::Dir {
                    name,
                    contents: Vec::new(),
                })
            }
            Some(x) => node.add(Node::File {
                name: words.next().unwrap().to_string(),
                size: x.parse::<i32>().unwrap(),
            }),
            None => {
                dbg!("no word found");
            }
        }
    }
    println!("part one {}", top.part_one());
    println!(
        "part two {}",
        top.part_two(30000000 - (70000000 - top.size()))
            .unwrap()
            .size()
    );
}

#[derive(Debug, Clone)]
enum Node {
    File { name: String, size: i32 },
    Dir { name: String, contents: Vec<Node> },
}

#[allow(unused_variables)]
impl Node {
    pub fn size(&self) -> i32 {
        match self {
            Node::File { name, size } => *size,
            Node::Dir { name, contents } => contents.iter().map(|x| x.size()).sum(),
        }
    }

    pub fn part_one(&self) -> i32 {
        match self {
            Node::File { name, size } => return *size,
            Node::Dir { name, contents } => {
                let size = self.size();
                let content_size = contents
                    .iter()
                    .map(|x| match x {
                        Node::Dir { name, contents } => x.part_one(),
                        _ => 0,
                    })
                    .sum();
                if size <= 100000 {
                    return size + content_size;
                } else {
                    return content_size;
                }
            }
        }
    }
    pub fn part_two<'a>(&'a self, free_space: i32) -> Option<&'a Self> {
        //full base case, so we can return self: this isn't big enough
        if self.size() < free_space {
            return None;
        }
        //base case: file
        let ret = match self {
            Node::File { name, size } => return None,
            //for each child: call part_two on it.
            //filter out the None
            //return the smallest one left
            // if none are left, return self
            Node::Dir { name, contents } => {
                contents
                    .iter()
                    .map(|x| x.part_two(free_space))
                    .filter(|x| x.is_some())
                    .filter(|x| x.as_ref().unwrap().size() > free_space)
                    .min_by_key(|x| x.as_ref().unwrap().size())

            }
        };
        if ret.is_some() {
            return ret.unwrap();
        } else {
            return Some(self);
        }
    }

    pub fn find<'a>(&'a mut self, dirname: &str) -> &'a mut Self {
        match self {
            Node::File { name, size } => {
                panic!("can't find descendants of a file");
            }
            Node::Dir { name, contents } => {
                for node in contents {
                    match node {
                        Node::File { name, size } => {}
                        Node::Dir { name, contents } => {
                            if name == dirname {
                                return node;
                            }
                        }
                    }
                }
                println!("failed to get into dir {dirname}");
                panic!("no dir found");
            }
        }
    }

    pub fn add<'a>(&mut self, other: Node) {
        match self {
            Node::File { name, size } => {
                panic!("added to a file")
            }
            Node::Dir { name, contents } => contents.push(other),
        }
    }

    pub fn name(&self) -> String {
        match self {
            Node::File { name, size } => name.clone(),
            Node::Dir { name, contents } => name.clone(),
        }
    }
}
